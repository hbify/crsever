const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const Schema = mongoose.Schema;
const hashSync = bcrypt.hashSync;
const compareSync =bcrypt.compareSync; 
//const constants = require('../../config/constants');
const  JWT_SECRET = 'MY_SECRET';
const UserSchema = new Schema(
  {
    email: {
      type: String,
      unique: true,
      required: [true, 'Email or phone is required!'],
      trim: true,
    },
    password: {
      type: String,
      required: [true, 'Password is required!'],
      trim: true,
      minlength: [6, 'Password need to be longer!']
    },
    role: { type: String, default: 'customer' },
    address: String,
    city: String,
    isAgent: Boolean,
    bookings: [{ type: Schema.Types.ObjectId, ref: 'Booking' }],
  },
  { timestamps: true },
);

UserSchema.pre('save', function(next) {
  if (this.isModified('password')) {
    this.password = this._hashPassword(this.password);
  }
  return next();
});

UserSchema.methods = {
  _hashPassword(password) {
    return hashSync(password);
  },
  authenticateUser(password) {
    return compareSync(password, this.password);
  },
  createToken() {
    return jwt.sign(
      {
        _id: this._id,
        role: this.role,
        agent: this.isAgent
      },
      JWT_SECRET,
    );
  },
  toAuthJSON() {
    return {
      _id: this._id,
      token: `JWT ${this.createToken()}`,
    };
  },
  toJSON() {
    return {
      _id: this._id,
    };
  }
};

module.exports = mongoose.model('User', UserSchema);

