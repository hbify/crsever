const devConfig = {
    MONGO_URL: 'mongodb://localhost/makeanodejsapi-dev',
    JWT_SECRET: '27cf8baa-a409-4cfe-b2bf-5ef19625af39',
  };
  
  const testConfig = {
    MONGO_URL: 'mongodb://localhost/makeanodejsapi-test',
  };
  
  const prodConfig = {
    MONGO_URL: 'mongodb://localhost/makeanodejsapi-prod',
  };
  
  const defaultConfig = {
    PORT: process.env.PORT || 3000,
  };
  
  function envConfig(env) {
    switch (env) {
      case 'development':
        return devConfig;
      case 'test':
        return testConfig;
      default:
        return prodConfig;
    }
  }
  
  exports = {
    ...defaultConfig,
    ...envConfig(process.env.NODE_ENV),
  };