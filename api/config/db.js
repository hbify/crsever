/* eslint-disable no-console */

const mongoose = require('mongoose');
const constants = require('./constants');
const dbURI = 'DB_URL';
// Remove the warning with Promise
mongoose.Promise = global.Promise;

// Connect the db with the url provide
try {
  mongoose.connect(dbURI, { useNewUrlParser: true ,  useCreateIndex: true }, );
} catch (err) {
  mongoose.createConnection(dbURI, { useNewUrlParser: true, useCreateIndex: true });
}

mongoose.connection
  .once('open', () => console.log('MongoDB Running'))
  .on('error', e => {
    throw e;
  });

require('../module/cars/car.model');
require('../module/agents/agent.model');
require('../module/bookings/booking.model');
require('../module/users/user.model');
require('../module/drivers/driver.model');