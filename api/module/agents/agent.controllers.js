const Agent = require('./agent.model');
//const Booking = require('../bookings/booking.model');


module.exports.agent =  (req, res, next) => {
   Agent.findById(req.body.id)
      .populate('cars')
      .populate('bookings')
      .exec((err, agent) => {
         if(err){
            return next(err);
         }
         res.status(200).json({agent});
      })
}

module.exports.search =  (req, res, next) => {
   Agent.find(req.body)
   .limit(10)
   .exec((err, agents) => {
      if (err){
         return next(err);
      }
      res.status(200).json({agents});
   })
}

 module.exports.createAgent =  (req, res, next) => {
   Agent.create(req.body)
      .then(agent => {
         res.status(200).json(agent);
      })
      .catch(next)
   
 }

 module.exports.updateAgent =  (req, res) => {
   Agent.findByIdAndUpdate(req.body.agentId, req.body.agent, {new: true},
      (err, agent) => {
         if (err){
            return next(err)
         }
         res.status(200).json({"message": "agent updated", "updated agent": agent});
      })
         
 }

 module.exports.deleteAgent =  (req, res, next) => {
    Agent.findByIdAndDelete(req.body.id)
         .exec((err, ag)=>{
            if(err){
               return next(err);
            }
            res.status(200).json({"message": "agent deleted", ag});
         }) 
 }
 
 /* module.exports.updateBooking = (req, res, next) => {
   Booking.findByIdAndUpdate(req.body.bookingId, req.body.booking, {new: true},
      (err, booking) => {
         if (err){
            return next(err)
         }
         res.status(200).json({"message": "booking updated", "updated booking": booking});
      })

} */

 module.exports.removeBooking = (req, res, next) => {
    agent = Agent.findById(req.body.agentId).populate(booking);
    agent.booking.splice(indexOf(req.body.bookingId))
    agent.save((err, agent)=>{
      if(err){ return next(err)}
      res.status(200).json({"message": "booking list updated", "updated agent after remove": agent});
    })

 }