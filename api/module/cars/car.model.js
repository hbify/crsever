var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;


var carsSchema = new Schema({
    name:String,
    type:String,
    passengers:Number,
    price:Number,
    luggage:Number,
    isAuto:Boolean,
    ACsup:Boolean,
    pickupLoc:String,
    insurance:Number,
    imageName:[String],
    isavailable:Boolean 
});



module.exports = mongoose.model('Car', carsSchema);
