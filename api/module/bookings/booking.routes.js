const express = require('express');
const routes = express.Router()


const userAuth = require('../../services/auth.services');
const bookingController = require('./booking.controllers');


routes.get('/', bookingController.index);
routes.put('/update', bookingController.update);
routes.post('/book', bookingController.book);
routes.post('/search', bookingController.search);
routes.delete('/delete', bookingController.delete);

module.exports = routes;
