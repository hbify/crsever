var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;


var agentsSchema = new Schema({
    name:String,
    tinNo: String,
    type:String,
    catagories: [String],
    user:  { type: Schema.Types.ObjectId, ref: 'User' },
    cars: [{ type: Schema.Types.ObjectId, ref: 'Car' }],
    bookings: [{ type: Schema.Types.ObjectId, ref: 'Booking' }],
});



module.exports = mongoose.model('Agent', agentsSchema);
