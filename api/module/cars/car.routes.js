const express = require('express');
const routes = express.Router()


//const userAuth = require('../../services/auth.services');
const carController = require('./car.controllers');

routes.post('/search', carController.search);
//routes.post('/getbyid', carController.getById);
routes.post('/add', carController.add);
routes.put('/update', carController.update);
routes.delete('/delete', carController.delete);


module.exports = routes;
