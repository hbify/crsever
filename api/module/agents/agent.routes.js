const express = require('express');
const routes = express.Router()


const userAuth = require('../../services/auth.services');
const agentController = require('./agent.controllers');

routes.post('/agent', agentController.agent);
routes.post('/search', agentController.search);
routes.post('/add', agentController.createAgent);
routes.put('/update', agentController.updateAgent);
routes.delete('/delete', agentController.deleteAgent);
//routes.post('/updatebooking', agentController.updateBooking);
routes.post('/removebooking', agentController.removeBooking);

module.exports = routes;
