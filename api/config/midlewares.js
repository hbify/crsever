const passport = require('passport');

//const isDev = process.env.NODE_ENV === 'development';
//const isProd = process.env.NODE_ENV === 'production';

export default app => {
  app.use(passport.initialize());
};