const Car = require('./car.model');

 module.exports.add =  (req, res, next ) => {
   Car.create(req.body)
      .then(car => {
         res.status(200).json(car);
      })
      .catch(next)
 }

 module.exports.search =  (req, res, next) => {
    Car.find(req.body)
      .exec((err, cars) => {
         if(err){
            return next(err)
         }
         res.status(200).json({"cars": cars});
      }) 
    
 }

 module.exports.update =  (req, res) => {
  Car.findByIdAndUpdate(req.body.carId, req.body.car, {new: true},
      (err,car) => {
         if (err){
            return next(err)
         }
         res.status(200).json({"message": "car updated", "updated car": car});
      })
 }

 module.exports.delete =  (req, res, next) => {
   Car.findByIdAndDelete(req.body.id)
         .exec((err, car)=>{
            if(err){
               return next(err);
            }
            res.status(200).json({"message": "Car deleted", car});
         }) 
}