const Booking = require('./booking.model');

module.exports.index =  (req, res, next) => {
   Booking.find()
      .exec((err, bookings) => {
         if(err){
            return next(err)
         }
         res.status(200).json({"bookings": bookings});
      })
 }

 module.exports.book =  (req, res, next ) => {
   Booking.create(req.body)
      .then(booking => {
         res.status(200).json(booking);
      })
      .catch(next)
 }

 module.exports.search =  (req, res, next) => {
    Booking.find(req.body)
      .exec((err, bookings) => {
         if(err){
            return next(err)
         }
         res.status(200).json({"bookings": bookings});
      }) 
    
 }

 module.exports.update =  (req, res) => {
  Booking.findByIdAndUpdate(req.body.bookingId, req.body.booking, {new: true},
      (err,booking) => {
         if (err){
            return next(err)
         }
         res.status(200).json({"message": "booking updated", "updated booking": booking});
      })
 }

 module.exports.delete =  (req, res, next) => {
   Booking.findByIdAndDelete(req.body.id)
         .exec((err, booking)=>{
            if(err){
               return next(err);
            }
            res.status(200).json({"message": "Booking deleted", booking});
         }) 
}