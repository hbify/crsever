const mongoose = require( 'mongoose' );
const Schema = mongoose.Schema;

const driverSchema = new Schema({
    driverName: String,
    phone: String,
    profilePhoto: String,
    licenceNo: String,
    licenceIssueDate: String,
    licenceExpiredDate: String,
    user: { type: Schema.Types.ObjectId, ref: 'User' },
})

module.exports = mongoose.model('Driver', driverSchema);