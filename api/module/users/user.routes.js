const express = require('express');
const routes = express.Router()
//import validate from 'express-validation';

const userAuth = require('../../services/auth.services');
const userController = require('./user.controllers');
//import userValidation from './user.validations';



//routes.post('/signup', validate(userValidation.signup), userController.signUp);
routes.post('/signup', userController.signUp);
routes.post('/login', userAuth.authLocal, userController.login);
routes.put('/update', userController.update);
routes.delete('/delete', userController.delete);
routes.post('/removebooking',userController.removeBooking)

module.exports = routes;
