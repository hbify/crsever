const User = require('./user.model');

module.exports.signUp = function (req, res) {
    User.create(req.body)
      .then(function(user){
          res.status(200).json(user.toAuthJSON());
    })
}

module.exports.login = function (req, res, next) {
  res.json(req.user.toAuthJSON());
  return next();
}

module.exports.update =  (req, res) => {
  User.findByIdAndUpdate(req.body.userId, req.body.user, {new: true},
      (err, user) => {
         if (err){
            return next(err)
         }
         res.status(200).json({"message": "user updated", "updated user": user});
      })
 }

 module.exports.delete =  (req, res, next) => {
   User.findByIdAndDelete(req.body.id)
         .exec((err, user)=>{
            if(err){
               return next(err);
            }
            res.status(200).json({"message": "User deleted", user});
         }) 
}

module.exports.removeBooking = (req, res, next) => {
  User.findById(req.body.userId)
          .populate(booking)
          .exec( (err, user) => {
            if(err){ 
              return next(err)
            }
            user.booking.splice(indexOf(req.body.bookingId))
            user.save((err, user)=>{
              if(err){ return next(err)}
              res.status(200).json({"message": "booking list updated", "updated user after remove": user});
            })
              

  })
  

}