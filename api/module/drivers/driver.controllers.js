const Driver = require('./driver.model');

 module.exports.add =  (req, res, next ) => {
   Driver.create(req.body)
      .then(driver => {
         res.status(200).json(driver);
      })
      .catch(next)
 }

 module.exports.search =  (req, res, next) => {
    Driver.find(req.body)
      .exec((err, drivers) => {
         if(err){
            return next(err)
         }
         res.status(200).json({"drivers": drivers});
      }) 
    
 }

 module.exports.update =  (req, res) => {
  Driver.findByIdAndUpdate(req.body.driverId, req.body.driver, {new: true},
      (err,driver) => {
         if (err){
            return next(err)
         }
         res.status(200).json({"message": "driver updated", "updated driver": driver});
      })
 }

 module.exports.delete =  (req, res, next) => {
   Driver.findByIdAndDelete(req.body.id)
         .exec((err, driver)=>{
            if(err){
               return next(err);
            }
            res.status(200).json({"message": "Driver deleted", driver});
         }) 
}