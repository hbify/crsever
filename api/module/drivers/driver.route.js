const express = require('express');
const routes = express.Router()


const userAuth = require('../../services/auth.services');
const driverController = require('./driver.controllers');

routes.post('/search', userAuth.authJwt, driverController.search);
routes.post('/add', userAuth.authJwt, driverController.add);
routes.put('/update', userAuth.authJwt, driverController.update);
routes.delete('/delete', userAuth.authJwt, driverController.delete);

module.exports = routes;
