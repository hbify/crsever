var mongoose = require( 'mongoose' );
var jwt = require('jsonwebtoken');
var Schema = mongoose.Schema;

var bookingSchema = new Schema(
    {
        pickupdate:String,
        dropoffdate:String,
        pickuploc:String,
        dropoffloc:String,
        price:Number,
        carid:String,
        status: String,
        user:  { type: Schema.Types.ObjectId, ref: 'user' },
        agent:  { type: Schema.Types.ObjectId, ref: 'Agent' },
        car: { type: Schema.Types.ObjectId, ref: 'Car' },
    }
);

module.exports = mongoose.models.Booking || mongoose.model('Booking', bookingSchema);
