const userRoutes= require('./users/user.routes');
const express = require('express');
const router = express.Router()
const carRoutes= require('./cars/car.routes');
const bookingRoutes= require('./bookings/booking.routes');
const agentRoutes= require('./agents/agent.routes');
const userAuth = require('../services/auth.services');
const driverRoutes = require('./drivers/driver.route');
const uploadRoutes = require('../module/imageUploads/uploads.route')

router.use('/v1/users', userAuth.authJwt, userRoutes);
router.use('/v1/cars', userAuth.authJwt, carRoutes);
router.use('/v1/bookings', userAuth.authJwt, bookingRoutes);
router.use('/v1/agents',userAuth.authJwt, agentRoutes);
router.use('/v1/drivers', userAuth.authJwt, driverRoutes);
router.use('/v1/images', userAuth.authJwt, uploadRoutes);

module.exports = router;